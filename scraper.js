const rp = require('request-promise')
const $ = require('cheerio')
const fs = require('fs')

var url = 'https://dwarffortresswiki.org/index.php/DF2014:Creature_token'
// const url = 'https://dwarffortresswiki.org/index.php/DF2014:Ammo_token'

rp(url)
  .then(function (html) {
    let tagname; let tagdesc
    const pattern = /\n/g
    const data = {}
    let tagargs = []
    const cleanHTML = $('*', html).removeAttr('style') // remove unnneeded inline styles
    // retrieves the rows that have tag anchors, doesn't work with non-tag tables like Ammo_token
    $('tr', cleanHTML).has('.text-anchor').each(function () {
      // token_rows.push($(this)); //used for fetching the raw tables
      // console.log($(this).html());
      // access the first and last cells in the row, strip the newline
      tagname = $(this).children(':first-child').text().replace(pattern, '')
      tagdesc = $(this).children(':last-child').text().replace(pattern, '')

      tagargs = []
      const argscell = $(this).children(':last-child').prev()
      if (argscell.has('ul')) {
        argscell.find('li').each(function () {
          // Object.assign(tagargs,{
          // [$(this).text()]: ""
          // ])
          const arg = {
            name: $(this).text(),
            data: 'string'
          }
          tagargs.push(arg)
        })
      } else {
        Object.assign(tagargs, {
          [argscell.text()]: ''
        })
      }
      Object.assign(data, {
        [tagname]: {
          args: tagargs,
          description: tagdesc
        }
      })
    })
    // token_rows[0].children(':first-child').text()

    // access every tr, check length of children (td)

    // example for ammo_token table, it's the only table in the page
    // console.log($('table',html).eq(0).html())

    fs.writeFile('./output/scraper_out.json', JSON.stringify(data), function (err) {
      if (err) throw err
    })
  })
  .catch(function (err) {
    throw err // handle error
  })

'use strict'

const chevrotain = require('chevrotain')

// ----------------- lexer -------------------
const createToken = chevrotain.createToken
const Lexer = chevrotain.Lexer
const CstParser = chevrotain.CstParser

// using createToken API
// const Boundingspace = createToken({
//     name: "Boundingspace",
//     pattern: /(?<=\n)\s+|\s+(?=\n)|\n/,
//     group: chevrotain.Lexer.SKIPPED
// })
const Filename = createToken({ name: 'Filename', pattern: /item_ammo|item_armor/ })
const Lbracket = createToken({ name: 'Lbracket', pattern: /\[/ })
const Rbracket = createToken({ name: 'Rbracket', pattern: /\]/ })
const Separator = createToken({ name: 'Separator', pattern: /:/ })
// const Tag = createToken({ name: 'Tag', pattern: /(?<=\[)[A-Z_]+(?=\]|:)/ })
// const Argument = createToken({ name: 'Argument', pattern: /(?<=:).*(?=:)|(?<=:).*(?=\])/ }) //Alternate: /(?=:)[^:]+(?=:|\])/
const Argument = createToken({ name: 'Argument', pattern: /[^:[\]\n]+/ })
const Newline = createToken({ name: 'Newline', pattern: /\n+/ })
// const Comment = createToken({
//     name: 'Comment',
//     pattern: /[^\[\]\n]+/,
//     group: chevrotain.Lexer.SKIPPED }) //optionally skip comments

const allTokens = [
  // BoundingSpace,
  Filename,
  Lbracket,
  Rbracket,
  Separator,
  // Tag,
  Argument,
  Newline
  // Comment
]

const RawLexer = new Lexer(allTokens) // TODO enable only offset tracking

// ------------- parser -------------
class RawParser extends CstParser {
  constructor () {
    super(allTokens, { recoveryEnabled: true })

    // for conciseness
    const $ = this

    $.RULE('rawFile', () => {
      $.CONSUME(Filename)
      $.OPTION(() => {
        $.SUBRULE($.comment)
      })
      $.SUBRULE($.contents)
    })

    $.RULE('contents', () => {
      $.MANY(() => {
        $.SUBRULE($.tagDef)
        $.OPTION(() => {
          $.SUBRULE($.comment)
        })
      })
    })
    // the lexer tokenizes the comments but the parser discards them as it depends more on where they are located than their structure
    $.RULE('comment', () => {
      $.MANY(() => {
        $.OR([
          { ALT: () => $.CONSUME(Newline) },
          { ALT: () => $.CONSUME(Argument) }
        ])
      })
    })

    $.RULE('tagDef', () => {
      $.CONSUME(Lbracket)
      $.CONSUME(Argument) // this is the tag name
      $.MANY(() => {
        $.SUBRULE($.argDef)
      })
      $.CONSUME(Rbracket)
    })

    $.RULE('argDef', () => {
      $.CONSUME(Separator)
      $.CONSUME(Argument) // these are the arguments
    })

    // very important to call this after all the rules have been defined.
    // otherwise the parser may not work correctly as it will lack information
    // derived during the self anaylsis phase
    this.performSelfAnalysis()
  }
}

// reuse the same parser instance.
const parser = new RawParser()

// wrapping it all together
module.exports = {
  parse: function parse (text) {
    const lexResult = RawLexer.tokenize(text)

    // setting a new input will RESET the parser instance's state
    parser.input = lexResult.tokens

    // any top level rule may be used as an entry point
    const cst = parser.rawFile()

    return {
      cst: cst,
      lexResult: lexResult,
      parseObject: parser
    }
  },
  Filename,
  Lbracket,
  Rbracket,
  Separator,
  Argument
}

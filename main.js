const parseRaws = require('./DFlang.js').parse
const fs = require('fs')

const inputText = `item_ammo

[OBJECT:ITEM_AMMO] thisisacomment

    [ITEM_AMMO:AMMO_ARROW]
    
    [WOOD][SPEED:123]
    [SIZE:60:100:200]
    somecomment`

const parsingResult = parseRaws(inputText)

// console.log(JSON.stringify(parsingResult.lexResult))

fs.writeFile('./output/lexer_out.json', JSON.stringify(parsingResult.lexResult), function (err) {
  if (err) throw err
})

fs.writeFile('./output/parser_out.json', JSON.stringify(parsingResult.parseObject), function (err) {
  if (err) throw err
})

console.log(parsingResult.lexResult.errors)
console.log(parsingResult.parseObject.errors)

fs.writeFile('./output/cst_out.json', JSON.stringify(parsingResult.cst), function (err) {
  if (err) throw err
})

'use strict'

const chevrotain = require('chevrotain')

// ----------------- lexer -------------------
const createToken = chevrotain.createToken
const Lexer = chevrotain.Lexer
// using createToken API
// const Boundingspace = createToken({
//     name: "Boundingspace",
//     pattern: /(?<=\n)\s+|\s+(?=\n)|\n/,
//     group: chevrotain.Lexer.SKIPPED
// })

const Filename = createToken({ name: 'Filename', pattern: /item_ammo|item_armor/ })
const Lbracket = createToken({ name: 'Lbracket', pattern: /\[/ })
const Rbracket = createToken({ name: 'Rbracket', pattern: /\]/ })
const Separator = createToken({ name: 'Separator', pattern: /:/ })
// const Tag = createToken({ name: 'Tag', pattern: /(?<=\[)[A-Z_]+(?=\]|:)/ })
// const Argument = createToken({ name: 'Argument', pattern: /(?<=:).*(?=:)|(?<=:).*(?=\])/ }) //Alternate: /(?=:)[^:]+(?=:|\])/
const Argument = createToken({ name: 'Argument', pattern: /[^:[\]\n]+/ })
const Newline = createToken({ name: 'Newline', pattern: /\n+/ })
// const Comment = createToken({
//     name: 'Comment',
//     pattern: /[^\[\]\n]+/,
//     group: chevrotain.Lexer.SKIPPED
// })

const allTokens = [
  // BoundingSpace,
  Filename,
  Lbracket,
  Rbracket,
  Separator,
  // Tag,
  Argument,
  Newline
  // Comment
]

const RawLexer = new Lexer(allTokens) // TODO enable only offset tracking

/// /////////////////////////////////////////////////////////////////////
const tokenVocabulary = {}

allTokens.forEach(tokenType => {
  tokenVocabulary[tokenType.name] = tokenType
})

module.exports = {
  tokenVocabulary: tokenVocabulary,

  lex: function (inputText) {
    const lexingResult = RawLexer.tokenize(inputText)

    if (lexingResult.errors.length > 0) {
      throw Error('Lexing errors detected')
    }

    return lexingResult
  }
}

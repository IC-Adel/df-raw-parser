# DF Raw Parser

This unfinished project is the result of trying to find a viable way of parsing Dwarf Fortress raw files.
The project is heavily based on the Chevrotain tutorial.

The scraper.js file scrapes the Creature tokens page of the Dwarf Fortress Wiki using Cheerio and outputs a structure that could potentially be used for 

There are two main.js files: 
- On the project root the main.js file runs DFLang.js for the purpose of visualizing lexer and parser modifications through output files.
- The src folder contains the most advanced version of the code and only outputs the resulting AST.

### SCRAPER > needs html

**Scraper access example:**
```sh
 df["CLUTCH_SIZE"].args[0].name
 df["CLUTCH_SIZE"].args[0].data
 ```

### LEXER > needs raw file
- filename in the first line
- check [ for tag opening
- check ] for tag close
- check : for delimiter
- everything else is text

### PARSER > needs lexer structure
- classify text into comment, tag or argument
- first token is filetype
- discard comments

### VISITOR > needs parser structure
- discard CST metadata
- build AST

**AST access example:**
```sh
rawAST.filetype
rawAST.tags[0].name
rawAST.tags[0].startColumn
rawAST.tags[0].args[0].value
rawAST.tags[0].args[0].startColumn
```

### TAG VALIDATOR > needs AST structure and scraped structure
- lookup token name in the hierarchy based on parent object type
- validate arguments based on hierarchy rules
- build symbol table

argument cases to validate
- integer (integer range will be separate properties)
- data type (boolean, string)
- object reference
- data array [DIURNAL, NOCTURNAL, CREPUSCULAR]
- builtin keyword (STP)
- argument values? #ARG

- validate references to other objects
- check of duplicate IDs

> SOMETHING NEEDS TO HANDLE CASES LIKE MATERIAL DEFINITIONS WHERE THE TAG USED WILL DETERMINE THE ADDITIONAL AMOUNT OF ARGUMENTS THAT ARE USED
> BUT IN ARGUMENT DATATYPE VALIDATION WE CAN'T BE SURE IF THE DATATYPES WILL BE IN THE SAME PLACE AND NO PRIOR TAG VALIDATION IS DONE

### OBJECT VALIDATOR > needs AST and rule structure
- check if the objects have the required tags
- check for allowed tags before the scope closing tag

### VS CODE
- autocompletion for object reference, data array and builtin keyword
- support for goto object reference

### ERROR TYPES

LEX ERRORS
- error: argument type mismatch
- error: missing argument
- error: unexpected argument
- warning: object token not recognized

OBJECT ERRORS
- error: duplicate object IDs
- warning: unused object ID
- error: object ID not found
- warning: missing tokens
- info: missing tokens?


### RAW SUPERSET
- lookup/reference object properties $find(AMMO_ARROW.CLASS.value) $(AMMO_ARROW.SIZE)
- copy object tags $copy(DWARF.GAITS)
- perform math operations $(AMMO_ARROW.SIZE[0]*0.2)
- declare file variables $default_size=100; $common_name="urist";
- use variables $(default_size/2)